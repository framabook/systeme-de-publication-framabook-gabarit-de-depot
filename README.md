Ce dépôt est basé sur l’utilisation du [Système de publication Framabook](https://framagit.org/framabook/systeme-publication-framabook).

Titre :

- ?

Auteurices : 

- ?

Éditeurices :

- ?

Fabricateurice :

- ?

Date de sortie :

- ?

ISBN :

- 1234567890123 (format)

Licence de l’ouvrage :

- Avec lien, exemple : [Creative Commons BY SA](https://creativecommons.org/licenses/by-sa/2.0/fr/)

Sujet:

- programmation, informatique, C, manuel (exemples)

Résumé (une ou deux phrases):

- ?

Version:

- ?
